<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImageUpload;
use Illuminate\Support\Facades\Auth;

class ImageUploadController extends Controller
{
  public function fileCreate() {
    return view('imageupload');
  }
  public function fileStore(Request $request){
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images'),$imageName);
        
        $imageUpload = new ImageUpload();
        $imageUpload->filename = $imageName;
        error_log(Auth::id());
        $imageUpload->username = Auth::id();
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
  }
}
