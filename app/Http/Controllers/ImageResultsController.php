<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ImageResultsController extends Controller
{
    //
    public function fileRetrieve() {
       $files = DB::table('image_uploads')->select('filename')->where('username', Auth::id())->get();
       return View::make('imageresults', ['files' => $files]);
    }
}
